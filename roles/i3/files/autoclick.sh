#!/bin/bash
eval $(xdotool getmouselocation --shell)
echo $X $Y
x1=$X
y1=$Y
COUNTER=1
COUNTEND=2
#REPEAT= 0.09s
while [ $COUNTER -lt $COUNTEND ]; do
  #echo the counter is $COUNTER
  xdotool click 1
  eval $(xdotool getmouselocation --shell)
  # cancel if mouse moved
  if [ $x1 != $X ] || [ $y1 != $Y ]; then
    #echo Mouse moved - script terminated
    exit 1
  fi
  #this sleep works for repetition rate
  #sleep 0.01s
  #let COUNTER=COUNTER+1
#  xdotool click --delay 90 --repeat 1000 1
done
