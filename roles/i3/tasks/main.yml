- name: Install required packages
  package:
    name: [i3lock-color, i3status, picom, jsoncpp, polkit, redshift,
          feh, python-pywal, imagemagick, gnome-control-center, gdm,
          gnome-flashback, gnome]
    state: present

- name: Enabled GDM and bluetooth
  service:
    name: "{{ item }}"
    enabled: yes
  with_items:
    - gdm
    - bluetooth

- name: Install packages for i3
  aur:
    skip_installed: true
    name:
      - i3-gaps
      - betterlockscreen
      - gscreenshot
      - i3-gnome
      - networkmanager-dmenu-git
  become: yes
  become_user: aur_builder

- name: Make required directories
  file:
    path: "/home/{{ username }}/{{ item }}"
    state: directory
    owner: "{{ username }}"
    group: "{{ username }}"
    mode: u=rwx,g=rx,o=rx
  with_items:
    - .config/i3
    - .config/i3/picom

- name: Copy i3 config files
  copy:
    src: ../files/{{ item }}
    dest: "/home/{{ username }}/.config/i3/{{ item }}"
    owner: "{{ username }}"
    group: "{{ username }}"
    mode: u=rw,g=r,o=r
  with_items:
    - config
    - wallpaper.png

- name: Set username in i3 config
  replace:
    path: "/home/{{ username }}/.config/i3/config"
    regexp: "username"
    replace: "{{ username }}"

- name: Set filemanager to ranger
  replace:
    path: "/home/{{ username }}/.config/i3/config"
    regexp: "ansible_filemanager"
    replace: "bindsym $mod+Shift+Return exec xfce4-terminal -e ranger"
  when: filemanager == "ranger"

- name: Set filemanager to nautilus
  replace:
    path: "/home/{{ username }}/.config/i3/config"
    regexp: "ansible_filemanager"
    replace: "bindsym $mod+Shift+Return exec nautilus"
  when: filemanager == "nautilus"

- name: Copy i3 script files
  copy:
    src: ../files/{{ item }}
    dest: "/home/{{ username }}/.config/i3/{{ item }}"
    owner: "{{ username }}"
    group: "{{ username }}"
    mode: u=rwx,g=rx,o=rx
  with_items:
    - passmenu-fixed.sh
    - xidlelock-run.sh
    - autoclick.sh

- name: Copy over Compton config
  copy:
    src: ../files/picom.conf
    dest: "/home/{{ username }}/.config/i3/picom/picom.conf"
    owner: "{{ username }}"
    group: "{{ username }}"
    mode: u=rw,g=r,o=r

- name: Add i3 to xinit
  copy:
    src: ../files/xinitrc
    dest: /etc/X11/xinit/xinitrc
    owner: root
    group: root
    mode: u=rw,g=r,o=r

- name: "Make tee passwordless for {{ username }}"
  lineinfile:
    dest: /etc/sudoers
    state: present
    regexp: "^{{ username }} ALL=(ALL) NOPASSWD: /usr/bin/tee"
    line: "{{ username }} ALL=(ALL) NOPASSWD: /usr/bin/tee"
    validate: 'visudo -cf %s'
