<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wp-user' );

/** MySQL database password */
define( 'DB_PASSWORD', '-gFqAiHvqH?|pu@e' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:/run/mysqld/mysqld.sock' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Vn<$o/a|srd]=Y-RL~)Me4cG-r^=JxgvYA}XMy{0!lwKm(f}o>af1WwKf@jdCnJJ');
define('SECURE_AUTH_KEY',  'B-jr/#Y4EF.d[%?[zB*U`E,/ndZ>S!YfomI0P$)/44JYMY[<:sW&z{1jl-+P%>x ');
define('LOGGED_IN_KEY',    'ujq)H_V5ZS*s0o^9Q~ggh+T5Qfglj?G;]|.`+y-o@!Ww0=zwHSFAK87=;~@X}]/+');
define('NONCE_KEY',        '$fG_9<D%KrjJW! Br3$64Tb/pJpY9[M)]-,LcfVa0tw/A>j,AN<PsXKz`(G8f+F ');
define('AUTH_SALT',        'B&M{M7gEYd08wQT~42G!PzAIWN^H!^R1I1(B/}--*2X.Q>ohME@g-ElT},|8<?rO');
define('SECURE_AUTH_SALT', ',>Q!|vI*I$a-FVk|gC?ifpSF.K`TKa]PxAS%=7llz#M1h7r+Tj!}@9AHIE5)bsTM');
define('LOGGED_IN_SALT',   '~NYX(u-[Wz9_jFAGm.P_LcRnTL**4,--,!b-_9%7B~plUN,vLcjGd6l@%6df80JT');
define('NONCE_SALT',       ']eEWfZ{vcL3FArn|lFpsCjcn@I9q[}MqWyxgBN1_yBA2*PqIkQ;)b#W]ctj/`<sK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
