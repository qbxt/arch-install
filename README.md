# Hashfastr's Arch Linux Ansible Config

## How to use

1. Have a working Arch install
  - Doesn't need to be fancy just have the following packages installed and an
  internet connection, you can even start some of the process in the ArchISO but
  not everything will work there.
    - Git
    - Ansible
    - ansible-playbook
    - Python
    - Text editor of your choice (nano, vim, etc)
    - An open-ssh server running
      - Ansible works over ssh so make sure you can ssh into root@localhost from root, you need no other functionality.

2. Using the root account clone the repo make your configuration

```
  git clone git@gitlab.com:Hashfastr/arch-install.git arch-install`
  cd arch-install
  cp template.yml my-config.yml
```

  - Under the tag 'vars' you will see the variables you will need, it will look something like this, the variables are simple and can be though of as Python like.

```
vars:
  username: my-username-here
  hostname: my-hostname-here
  zsh_theme: bira
  filemanager: nautilus
  install_nerd_fonts: true
  wifi_interface: wlan0
```

  - Replace `my-username-here` and `my-hostname-here` with your respective username and hostname.
  - My install uses zsh as the shell, the `zsh_theme` variable sets the theme for the shell using oh-my-zsh, the default is bira, my favorite/
  - There are two filemanagers currently supported. Ranger, a terminal based file manager, and nautilus, Gnome's default graphical file manager.
  - `install_nerd_fonts` should be set to true for polybar and zsh to work properly, if you already have them installed then just set to false. The initial install will take some time depending on your internet but once it installs it will never do it again.
  - If you plan on using wifi place your interface name here so that polybar will correctly display it.
    - This can be found via `ip addr show` and generally starts with a `w` such as `wlan0` or `wlp5s0`.

  - Now we get to the meat of the configuration, the roles, listed here are the operations that will be performed. You can see available roles under the `roles` folder. In this example we will install and configure grub, install dev apps such as gcc and make, configure locales, users, the shell, the hostname, and install yay for access to the aur.

```
  roles:
    - grub
    - dev-apps
    - locales
    - users
    - shell
    - hostname
    - aur
    - network
```

Below is very generic install to incorporate all basic functionality.

```
  roles:
    - grub
    - dev-apps
    - locales
    - users
    - shell
    - hostname
    - aur
    - network
    - update-packages
    - radeon-graphics # replace with nvidia, intel-graphics, or ati-graphics by need
    - fonts
    - i3
    - ranger # not necessarily needed if using nautilus
    - polybar
    - rofi
    - xfce4-terminal
    - graphical_apps # Installs a number of graphical apps, a lot you may not need, but some essential.
    - audio
```

Other roles can be added to further make your life easier such as `virtualization` which will set up qemu and kvm, and along with `graphical-apps` will install virtual machine manager.
If you would like to just get the polybar config then just put in the polybar role, if you would just like the rofi config just include the rofi role, same goes for xfce4-terminal, same goes for i3 but that depends on polybar, rofi, and xfce4-terminal.
Below is just if you already have a graphical envrionment but would like to use my configuration.

```
  roles:
    - aur
    - fonts # do not need if you already have nerdfonts installed, preferably 2.0.0 as other versions can have spacing issues
    - i3
    - ranger # not needed if using nautilus
    - polybar
    - rofi
    - xfce4-terminal
```

# Warning
```
Some server roles are not generic or just plain don't work and are a result of my tinkering, examples are synapse and owncloud.
```

# TODO
1. Better differentiate roles
    - Move different graphical apps to their own roles
2. Make roles more general so that they work on not just Arch and not just on one architecture
3. Clean and organize
4. Add different graphical environments
    - i3-gnome used to just be i3, make the two separate for those who do not want gnome
5. Add different themes to polybar and i3 that can be selected using a variable in the configuration yml file
6. Improve neatness

# How you can help
1. Let me know if I left stuff in that is specific to me that can be generalized
    - Includes things like personal info etc
2. Add roles based on the Arch Linux Wiki
3. Generally improve generality of the roles
4. Improve the readme wording or formatting
5. Improve code neatness or improve code to better conventions

# Contact me
## Email: sylvainjones@hashfastr.com
## Discord: Hashfastr#3587
### Usually will be happy to help but also a busy student
